# **Базы данных в Андроид приложениях**

1. Особенности реляционных баз данных
2. Особенность SQLite (Self-contained, serverless, zero-configuration)
3. Основные концепции транзакции ACID – Atomicity, Consistency, Isolation, Durability
4. Операторы SQL
5. Основные SQL запросы C.R.U.D.
6. Типы данных в SQLite (NULL, INTEGER, REAL, TEXT, BLOB)
7. API для управления SQLite (Классы для CRUD и особенности работы с ними)
8. Библтотека Room как слой абстакции над SQLite
9. Преимущества Room
   - проверка SQL-запрорсов во время компиляции
   - незначитеьный шаблонный код
   - полная интеграция с другими компанентами архитектуры (например, LiveData)
10. Особенности использования Room
    - Основные компоненты
    - Аннотации

11. Проектирование баз данных
    - Нормальные формы и их соблюдение в проектировании
    - Правила Кодда и их соблюдение
12. Асинхронная работа с БД 
    - Использование LiveData
    - Использование RxJava 
13. Особенности работв с Realm как с NoSQL базой данных
