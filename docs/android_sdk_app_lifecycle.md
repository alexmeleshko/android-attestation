# Android SDK & App lifecycle

## Pre-Intermediate

(встречается почти в каждом проекте, обязательно знать для нормальной работы на типовых проектах )

######  Жизненные циклы Activity и Fragment (понимание ЖЦ, очередность срабатывания методов ЖЦ, взаимодействие кода приложения с методами ЖЦ, Fragment manager)

###### Context (виды, различия, выбор)

###### Передача данных между Activity и Fragment (А->A, A->F, F->F, F->A)

###### Permissions (виды, различия, различия в работе)

###### Manifest - настройка разрешений, работа с компонентами

###### Архитектура

- Android Architecture Components (basics, ViewModel, LiveData)
- Понимание базовых концепций

###### Хранение данных (файловая система, SharedPreferences)

###### Network

- OkHttp - отправка и получение сетевых запросов на основе HTTP
- Gson / Moshi - преобразование объектов Java в их представление JSON и наоборот.
- Retrofit 2 - типобезопасный HTTP-клиент

###### Работа с изображениями (асинхронная загрузка из сети, ресурсов и файловых систем, их кэширование и отображение)

- Picasso
- Glide

###### Асинхронная работа 

- концептуальное понимание
- умение ориентироваться в документации Rx
- Multithreading (Looper, AsynkTask,Handler)

###### Dependency injection

- Dagger 2
- Hilt

###### Базы данных

- Room
- Realm
- SQLite

###### Firebase

- Authentication
- Crashlytics

###### Gradle

###### Иметь опыт заливки в Google Play

###### Инструменты и сервисы для тестирования специфичные для андроид

###### Навык использования ADB bridge

###### Опыт использования эмуляторов

## Intermediate

Может не на каждом проекте но обязательно встретится. Нужно знать и иметь представления для того, чтобы справляться с задачами полноценно, самостоятельно, и в любом  этапе 
цикла.

###### Работа с изображениями (асинхронная загрузка из сети, ресурсов и файловых систем, их кэширование и отображение)

- Picasso
- Glide

###### Асинхронная работа

- RxJava 2

###### Dependency injection

- Dagger 2
- Hilt
- Koin (for Kotlin)

###### Runtime permissions

- Dexter
- RxPermissions

###### Firebase

- Authentication
- Crashlytics
- Cloud messaging

###### Архитектурная специфика Андроид приложений

- Android Architecture Components (deep knowledge + WorkManager)
- MVP, VIPER, MVI

###### Сервисы (виды, отличие работы в разных версиях Android API)

###### Notification Manager

###### BroadcastReceiver

###### Content Providers

###### Deep knowledge of FS and Storage APIs for Android

###### Network Capabilities

###### Android Jetpack

###### Глубокое понимание или навык автоматизации ADB bridge

###### Опыт использования специфичных CI/CD tools & services

- Fastlane
- BitRise

###### Gradle in depth

## Upper

Те навыки и экспертиза которые специфичны для определённых отраслей или задач, которые редко встречаются. Технологии или знания которые не являются критичными для выполнения всего цикла работ.

###### Работа с сенсорами

###### Advanced network

###### Android NDK

###### JNI

###### IOT

###### TF lite

###### Vibrator, AudioManager, MediaPlayer

###### Android TV

###### Deep skill with CI/CD tools & services

- Gitlab CI
- Jenkins

## Special KA

###### Java ?

###### Kotlin

###### Android SDK & App lifecycle

###### WEB & HTTP

###### Databases(Nosql+RDBMS)
